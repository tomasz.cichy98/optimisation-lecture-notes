import numpy as np
import matplotlib.pyplot as plt

n = 1
s_total = 0
s_list = []
while n <= 200:
    s_total += 1/n**2
    s_list.append(s_total)

    n += 1

print(f"S final:\t {s_total}")
print(f"s_k:\t\t {s_list[-1]}")

k = np.arange(1, 201, 1)
plt.plot(k, s_list)
plt.xlabel("$x$")
plt.ylabel("$s_k$")
plt.title(r"$$")
plt.savefig("prob2.pdf")
plt.show()