import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 5, 50)

plt.figure()
plt.plot(x, np.sqrt(x))
plt.xlim(0, 5)
plt.ylim(-5,5)
plt.xlabel("x")
plt.ylabel("y")
plt.title(r"$f(x) = \sqrt{x}$")
plt.show()
plt.savefig("prob1.pdf")