import numpy as np
import matplotlib.pyplot as plt

# part a
def steep_desc(Q, b, x0, eps):
    n = 0
    x = x0
    x_list = [x]
    graf_f_list = list()
    grad_f_norm = 100
    
    while grad_f_norm > eps:
        g = np.dot(Q, x) - b
        
        # get alpha
        Q_g = np.dot(Q, g)
        alpha = np.dot(g, g)/np.dot(g, Q_g)
        
        # get new x
        x = x - (alpha*g)
        
        x_list.append(x)
        grad_f_norm = np.linalg.norm(g)
        graf_f_list.append(grad_f_norm)
        
        n += 1
        # print(f"x = {x}, g = {g}, grad_f_norm = {grad_f_norm}, alpha = {alpha}")
    return np.array(x_list), np.array(graf_f_list)

x0 = np.array([3, 4])
eps = 1e-8
Q = np.array([[4, -1], [-1,1]])
b = np.array([1, -1])

# X, G = grad_desc(f, x0, eps, alpha)
X, G = steep_desc(Q, b, x0, eps)

# print(f"Minimum is for x = {X[-1, 0]}, {X[-1,1]}")

# part b
N = 100
x1_i = np.linspace(-5, 5, N)
x2_i = np.linspace(-5, 5, N)
x1, x2 = np.meshgrid(x1_i, x2_i)

f = 2*x1**2 + 1/2*x2**2 - x1*x2 - x1 + x2

plt.contour(x1, x2, f, 40)
plt.plot(X[:,0], X[:,1], "C3o-")

plt.colorbar()
plt.xlabel(r"$x_1$")
plt.ylabel(r"$x_2$")
plt.xlim(-5,5)
plt.ylim(-5,5)
plt.title(r"Contour of $f(\vec{x}) = 2x_1^2 + \frac{1}{2}x_2^2 - x_1x_2 - x_1 + x_2 $")
plt.savefig("prob2.pdf")