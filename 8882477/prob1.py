import numpy as np
import matplotlib.pyplot as plt

N = 200
x1_i = np.linspace(-3,3, N)
x2_i = np.linspace(-3,3, N)
x1, x2 = np.meshgrid(x1_i, x2_i)
f = x1*x2

# part a
plt.contour(x1, x2, f, 40)

# part b
theta = np.linspace(0,2*np.pi,100)
r = np.sqrt(8/(np.cos(theta)**2 + 4*np.sin(theta)**2)) 
h_x1 = r * np.cos(theta)
h_x2 = r * np.sin(theta)
plt.plot(h_x1, h_x2, "k", label=r"$g(x,y) = \frac{x^2}{8} + \frac{y^2}{2} = 1$")

# part c
plt.plot(2, 1, "C4o", label="(2, 1)")
plt.plot(-2, 1, "C5o", label="(-2, 1)")

# part d

# point (2, 1)
# grad f
plt.quiver(2, 1, 1, 2, angles="xy", scale_units="xy", scale=2, color="C1")
# grad g
plt.quiver(2, 1, 2/4, 1, angles="xy", scale_units="xy", scale=2, color="C0")

# point (-2, 1)
# grad f
plt.quiver(-2, 1, 1, -2, angles="xy", scale_units="xy", scale=2, color="C1")
# grad g
plt.quiver(-2, 1, -2/4, 1, angles="xy", scale_units="xy", scale=2, color="C0")

plt.colorbar()
plt.title(r"Contour of $f(x,y) = xy$ with g(x) and vectors")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.xlim(-3,3)
plt.ylim(-3,3)
plt.legend(loc="lower left")
plt.savefig("prob1.pdf")